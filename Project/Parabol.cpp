#include "Parabol.h"

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	//draw 2 points
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
}
void BresenhamDrawParabolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	int x = 0, y = 0;
	int A2 = 2 * A;
	int d = 1 - A;
	while (x < A && y <= 600)
	{
		Draw2Points(xc, yc, x, y, ren);
		if (d >= 0)
		{
			y++;
			d += -A2;
		}
		d += 2 * x + 1;
		x++;
	}
	if (d == 1)
		d = 1 - (2 * A2);
	else
		d = 1 - A2;
	while (y <= 600)
	{
		Draw2Points(xc, yc, x, y, ren);
		if (d <= 0)
		{
			d += 4 * x;
			x++;
		}
		d += -(2 * A2);
		y++;
	}
}

void BresenhamDrawParabolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	int p = -A;
	int x = 0, y = 0;
	Draw2Points(xc, yc, x, y, ren);
	while (x <= A && y >= -600)
	{
		if (p > 0)
		{
			p += -(2 * x + 1);
		}
		else
		{
			p += 2 * A - (2 * x + 1);
			y--;
		}
		x++;
		Draw2Points(xc, yc, x, y, ren);
	}
	p = 2 * x * x + 4 * A * y - 2 * x - 1;
	while ((x > A) && (y >= -600))
	{
		if (p > 0)
		{
			p += -4 * A;
		}
		else
		{
			p += 4 * x + 4 - 4 * A;
			x++;
		}
		y--;
		Draw2Points(xc, yc, x, y, ren);
	}
}
