#include "DrawPolygon.h"
#include <iostream>
using namespace std;

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[3], y[3];
	float phi = M_PI / 2;

	for (int i = 0; i < 3; i++)
	{
		x[i] = xc + round(R * cos(phi));
		y[i] = yc - round(R * sin(phi));
		phi += 2 * M_PI / 3;
	}

	for (int i = 0; i < 3; i++)
	{
		DDA_Line(x[i], y[i], x[(i + 1) % 3], y[(i + 1) % 3], ren);
	}
}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[4], y[4];
	float phi = 0;

	for (int i = 0; i < 4; i++)
	{
		x[i] = xc + round(R* cos(phi));
		y[i] = yc - round(R* sin(phi));
		phi += M_PI / 2;
	}

	for (int i = 0; i < 4; i++) 
	{
		DDA_Line(x[i], y[i], x[(i + 1) % 4], y[(i + 1) % 4], ren);
	}
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5];
	float phi = M_PI / 2;

	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + round(R* cos(phi));
		y[i] = yc - round(R *sin(phi));
		phi += 2 * M_PI / 5;
	}

	for (int i = 0; i < 5; i++)
	{
		DDA_Line(x[i], y[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[6], y[6];
	float phi = M_PI / 2;

	for (int i = 0; i < 6; i++)
	{
		x[i] = xc + round(R* cos(phi));
		y[i] = yc - round(R* sin(phi));
		phi += M_PI / 3;
	}

	for (int i = 0; i < 6; i++)
	{
		DDA_Line(x[i], y[i], x[(i + 1) % 6], y[(i + 1) % 6], ren);
	}
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5];
	float phi = M_PI / 2;

	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + round(R* cos(phi));
		y[i] = yc - round(R *sin(phi));
		phi += 2 * M_PI / 5;
	}

	for (int i = 0; i < 5; i++)
	{
		DDA_Line(x[i], y[i], x[(i + 2) % 5], y[(i + 2) % 5], ren);
	}
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{

	int x[5], y[5], xnho[5], ynho[5];
	float phi = M_PI / 2;

	float R_nho = R * sin(M_PI / 10) / sin(7 * M_PI / 10);

	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + round(R* cos(phi));
		y[i] = yc - round(R* sin(phi));
		xnho[i] = xc + round(R_nho * cos(phi + M_PI / 5));
		ynho[i] = yc - round(R_nho * sin(phi + M_PI / 5));
		phi += 2 * M_PI / 5;
	}

	for (int i = 0; i < 5; i++)
	{
		DDA_Line(x[i], y[i], xnho[i], ynho[i], ren);
		DDA_Line(xnho[i], ynho[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[8], y[8];
	float phi = M_PI / 2;

	for (int i = 0; i < 8; i++)
	{
		x[i] = xc + round(R* cos(phi));
		y[i] = yc - round(R *sin(phi));
		phi += 2 * M_PI / 8;
	}

	for (int i = 0; i < 8; i++)
	{
		DDA_Line(x[i], y[i], x[(i + 2) % 8], y[(i + 2) % 8], ren);
	}
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
	int x[5], y[5], xn[5], yn[5];
	float phi = startAngle;

	float R_nho = R * sin(M_PI / 10) / sin(7 * M_PI / 10);

	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + round(R* cos(phi));
		y[i] = yc - round(R* sin(phi));
		xn[i] = xc + round(R_nho * cos(phi + M_PI / 5));
		yn[i] = yc - round(R_nho * sin(phi + M_PI / 5));
		phi += 2 * M_PI / 5;
	}

	for (int i = 0; i < 5; i++)
	{
		DDA_Line(x[i], y[i], xn[i], yn[i], ren);
		DDA_Line(xn[i], yn[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
	if (r <= 0)
		return;

	float phi = M_PI / 2;
	
	DrawStarAngle(xc, yc, r, phi, ren);
	float R_nho = r * sin(M_PI / 10) / sin(7 * M_PI / 10);
	DrawConvergentStar(xc, yc, R_nho, ren);
}