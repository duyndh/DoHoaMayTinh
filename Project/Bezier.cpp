#include "Bezier.h"
#include <iostream>
using namespace std;

bool isInsideRect(SDL_Rect rect, Vector2D point)
{
	return (point.x >= rect.x) && (point.x <= rect.x + rect.w)
		&& (point.y >= rect.y) && (point.y <= rect.y + rect.h);
}

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	Vector2D curPoint;
	Vector2D prePoint(p1.x * pow(1 - 0, 2) + p2.x * 2 * (1 - 0) * 0 + p3.x * pow(0, 2),
					p1.y * pow(1 - 0, 2) + p2.y * 2 * (1 - 0) * 0 + p3.y * pow(0, 2));
	
	for (int it = 1; it <= 100; it++)
	{
		double t = it / 100.0;
		curPoint.x = p1.x * pow(1 - t, 2) + p2.x * 2 * (1 - t) * t + p3.x * pow(t, 2);
		curPoint.y = p1.y * pow(1 - t, 2) + p2.y * 2 * (1 - t) * t + p3.y * pow(t, 2);
		
		SDL_RenderDrawLine(ren, (int)(curPoint.x + 0.5f), (int)(curPoint.y + 0.5f), 
								(int)(prePoint.x + 0.5f), (int)(prePoint.y + 0.5f));
		prePoint = curPoint;
	}
}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	Vector2D curPoint;
	Vector2D prePoint(p1.x * pow(1 - 0, 3) + p2.x * 3 * pow(1 - 0, 2) * 0 + p3.x * 3 * (1 - 0) * pow(0, 2) + p4.x * pow(0, 3),
		p1.y * pow(1 - 0, 3) + p2.y * 3 * pow(1 - 0, 2) * 0 + p3.y * 3 * (1 - 0) * pow(0, 2) + p4.y * pow(0, 3));

	for (int it = 1; it <= 100; it++)
	{
		double t = it / 100.0;
		curPoint.x = p1.x * pow(1 - t, 3) + p2.x * 3 * pow(1 - t, 2) * t + p3.x * 3 * (1 - t) * pow(t, 2) + p4.x * pow(t, 3);
		curPoint.y = p1.y * pow(1 - t, 3) + p2.y * 3 * pow(1 - t, 2) * t + p3.y * 3 * (1 - t) * pow(t, 2) + p4.y * pow(t, 3);
		
		SDL_RenderDrawLine(ren, (int)(curPoint.x + 0.5f), (int)(curPoint.y + 0.5f),
			(int)(prePoint.x + 0.5f), (int)(prePoint.y + 0.5f));
		prePoint = curPoint;
	}
}
