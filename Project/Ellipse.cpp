#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc - y);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	// Area 1
	int a2 = pow(a, 2);
	int b2 = pow(b, 2);
	int x = 0, y = b;
	int p = 0;
	
	Draw4Points(xc, yc, x, y, ren);
	p = (int)(b2 - (a2 * b) + (a2 / 4));
	while ((2 * b2) * x < (2 * a2) * y)
	{
		x++;
		if (p < 0)
		{
			p += (int)(2 * b2) * x + b2;
		}
		else
		{
			y--;
			p += (int)(2 * b2) * x + b2 - (2 * a2) * y;
		}
		Draw4Points(xc, yc, x, y, ren);
	}
	// Area 2
	p = (int)(b2 * (x + 0.5) * (x + 0.5) + a2 * (y - 1) * (y - 1) - a2 * b2);
	while (y > 0)
	{
		y--;
		if (p > 0)
		{
			p += a2 - (2 * a2) * y;
		}
		else
		{
			x++;
			p += a2 + (2 * b2) * x - (2 * a2) * y;
		}
		Draw4Points(xc, yc, x, y, ren);
	}
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	int x = 0;
	int y = b;
	int p = b * b - a * a * b + a * a / 4;
	while (2.0 * b * b * x <= 2.0 * a * a * y)
	{
		if (p < 0)
		{
			p += 2 * b * b * x + b * b;
		}
		else
		{
			p += 2 * b * b * x - 2 * a * a * y - b * b;
			y--;
		}
		x++;
		Draw4Points(xc, yc, x, y, ren);
	}
	
	p = b * b * (x + 0.5) * (x + 0.5) + a * a * (y - 1) * (y - 1) - a * a * b * b;
	while (y > 0)
	{
		if (p <= 0)
		{
			p += 2 * b * b * x - 2 * a * a * y + a * a;
			x++;
		}
		else
		{
			p += - 2 * a * a * y + a * a;
		}
		y--;
		Draw4Points(xc, yc, x, y, ren);
	}
}
