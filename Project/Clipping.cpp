#include "Clipping.h"

RECT CreateWindow(int l, int r, int t, int b)
{
    RECT rect;
    rect.Left = l;
    rect.Right = r;
    rect.Top = t;
    rect.Bottom = b;

    return rect;
}

CODE Encode(RECT r, Vector2D P)
{
    CODE c = 0;
    if (P.x < r.Left)
        c = c|LEFT;
    if (P.x > r.Right)
        c = c|RIGHT;
    if (P.y < r.Top)
        c = c|TOP;
    if (P.y > r.Bottom)
        c = c|BOTTOM;
    return c;
}

int CheckCase(int c1, int c2)
{
    if (c1 == 0 && c2 == 0)
        return 1;
    if (c1 != 0 && c2 != 0 && c1&c2 != 0)
        return 2;
    return 3;
}

int CohenSutherland(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	int c1 = Encode(r, P1);
	int c2 = Encode(r, P2);
	int check = CheckCase(c1, c2);

	while (check == 3)
	{
		ClippingCohenSutherland(r, P1, P2);
	
		c1 = Encode(r, P1);
		c2 = Encode(r, P2);
		check = CheckCase(c1, c2);
	}
	if (check == 2)
		return 0;
	
	Q1 = P1;
	Q2 = P2;
	return 1;
}

void ClippingCohenSutherland(RECT r, Vector2D &P1, Vector2D &P2)
{
	int c1 = Encode(r, P1);
	int c2 = Encode(r, P2);
	
	int dx = P2.x - P1.x;
	int dy = P2.y - P1.y;

	float m = float(dy) / dx;

	if (!c1)
	{
		Vector2D tmp(P1);
		P1.set(P2);
		P2.set(tmp);

		c1 = Encode(r, P1);
		c2 = Encode(r, P2);
	}
	
	if (c1 & LEFT)
	{
		P1.y += m * (r.Left - P1.x);
		P1.x = r.Left;
	}
	else if (c1 & RIGHT)
	{
		P1.y += m * (r.Right - P1.x);
		P1.x = r.Right;
	}
	else if (c1 & TOP)
	{
		if (dx)
			P1.x += (r.Top - P1.y) / m;
		P1.y = r.Top;
	}
	else if (c1 & BOTTOM)
	{	
		if (dx)
			P1.x += (r.Bottom - P1.y) / m;
		P1.y = r.Bottom;
	}
}

int SolveNonLinearEquation(int p, int q, float &t1, float &t2)
{
    if (p == 0)
    {
        if (q < 0)
            return 0;
        return 1;
    }

    if (p > 0)
    {
        float t = (float)q / p;
        if (t2 < t)
            return 1;
        if (t < t1)
            return 0;
        t2 = t;
        return 1;
    }

    float t = (float)q / p;
    
	if (t2 < t)
        return 0;
    if(t < t1)
        return 1;

    t1 = t;
    return 1;
}

int LiangBarsky(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	int xmin = r.Left;
	int xmax = r.Right;
	int ymin = r.Top;
	int ymax = r.Bottom;

	int dx = P2.x - P1.x;
	int dy = P2.y - P1.y;

	float t1 = 0, t2 = 1;

	int q1 = SolveNonLinearEquation(-dx, P1.x - xmin, t1, t2);
	int q2 = SolveNonLinearEquation(dx, xmax - P1.x, t1, t2);
	int q3 = SolveNonLinearEquation(-dy, P1.y - ymin, t1, t2);
	int q4 = SolveNonLinearEquation(dy, ymax - P1.y, t1, t2);
	
	if (q1 * q2 * q3 * q4)
	{
		Q1.x = P1.x + t1 * dx;
		Q1.y = P1.y + t1 * dy;
		Q2.x = P1.x + t2 * dx;
		Q2.y = P1.y + t2 * dy;
		return 1;
	}

	return 0;
}