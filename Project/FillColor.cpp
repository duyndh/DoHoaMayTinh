#include "FillColor.h"
#include <queue>
#include <stack>
#include <iostream>
using namespace std;

#define INT_SIZE sizeof(Uint32) * 8 /* Integer size in bits */

int findHighestBitSet(Uint32 num)
{
	int order = 0, i;

	/* Iterate over each bit of integer */
	for (i = 0; i<INT_SIZE; i++)
	{
		/* If current bit is set */
		if ((num >> i) & 1)
			order = i;
	}

	return order;
}

//Get color of a pixel
SDL_Color getPixelColor(Uint32 pixel_format, Uint32 pixel)
{
	SDL_PixelFormat* fmt = SDL_AllocFormat(pixel_format);

	Uint32 temp;
	Uint8 red, green, blue, alpha;

	//Check if pixel is a 32-bit integer
	if (findHighestBitSet(pixel) == 31)
	{
		/* Get Alpha component */
		temp = pixel & fmt->Amask;  /* Isolate alpha component */
		temp = temp >> fmt->Ashift; /* Shift it down to 8-bit */
		temp = temp << fmt->Aloss;  /* Expand to a full 8-bit number */
		alpha = (Uint8)temp;
	}
	else {
		alpha = 255;
	}

	/* Get Red component */
	temp = pixel & fmt->Rmask;  /* Isolate red component */
	temp = temp >> fmt->Rshift; /* Shift it down to 8-bit */
	temp = temp << fmt->Rloss;  /* Expand to a full 8-bit number */
	red = (Uint8)temp;

	/* Get Green component */
	temp = pixel & fmt->Gmask;  /* Isolate green component */
	temp = temp >> fmt->Gshift; /* Shift it down to 8-bit */
	temp = temp << fmt->Gloss;  /* Expand to a full 8-bit number */
	green = (Uint8)temp;

	/* Get Blue component */
	temp = pixel & fmt->Bmask;  /* Isolate blue component */
	temp = temp >> fmt->Bshift; /* Shift it down to 8-bit */
	temp = temp << fmt->Bloss;  /* Expand to a full 8-bit number */
	blue = (Uint8)temp;


	SDL_Color color = { red, green, blue, alpha };
	return color;
}

//Get all pixels on the window
SDL_Surface* getPixels(SDL_Window* SDLWindow, SDL_Renderer* SDLRenderer) {
	SDL_Surface* saveSurface = NULL;
	SDL_Surface* infoSurface = NULL;
	infoSurface = SDL_GetWindowSurface(SDLWindow);
	if (infoSurface == NULL) {
		std::cerr << "Failed to create info surface from window in saveScreenshotBMP(string), SDL_GetError() - " << SDL_GetError() << "\n";
		return NULL;
	}
	else {
		unsigned char * pixels = new (std::nothrow) unsigned char[infoSurface->w * infoSurface->h * infoSurface->format->BytesPerPixel];
		if (pixels == 0) {
			std::cerr << "Unable to allocate memory for screenshot pixel data buffer!\n";
			return NULL;
		}
		else {
			if (SDL_RenderReadPixels(SDLRenderer, &infoSurface->clip_rect, infoSurface->format->format, pixels, infoSurface->w * infoSurface->format->BytesPerPixel) != 0) {
				std::cerr << "Failed to read pixel data from SDL_Renderer object. SDL_GetError() - " << SDL_GetError() << "\n";
				return NULL;
			}
			else {
				saveSurface = SDL_CreateRGBSurfaceFrom(pixels, infoSurface->w, infoSurface->h, infoSurface->format->BitsPerPixel, infoSurface->w * infoSurface->format->BytesPerPixel, infoSurface->format->Rmask, infoSurface->format->Gmask, infoSurface->format->Bmask, infoSurface->format->Amask);
			}
		}
	}
	return saveSurface;
}

//Compare two colors
bool compareTwoColors(SDL_Color color1, SDL_Color color2)
{
	if (color1.r == color2.r && color1.g == color2.g && color1.b == color2.b && color1.a == color2.a)
		return true;
	return false;
}


bool canFilled(SDL_Window *win, Vector2D newPoint, Uint32 pixel_format,
	SDL_Renderer *ren, SDL_Color fillColor, SDL_Color boundaryColor)
{
	SDL_Surface *surface = getPixels(win, ren);

	//Convert the pixels to 32 bit
	Uint32 *pixels = (Uint32 *)surface->pixels;
	int w = surface->w;

	int index = newPoint.y * w + newPoint.x;
	Uint32 pixel = pixels[index];
	SDL_Color color = getPixelColor(pixel_format, pixel);
	cout << (int)color.r << "," << (int)color.g << "," << (int)color.b << "," << (int)color.a << endl;

	if (!compareTwoColors(color, fillColor) && !compareTwoColors(color, boundaryColor))
	{
		return true;
	}

	return false;
}

Uint32 get_pixel32(SDL_Surface *surface, int x, int y)
{
	Uint32 *pixels = (Uint32 *)surface->pixels;
	return pixels[(y * surface->w) + x];
}

void put_pixel32(SDL_Surface *surface, int x, int y, Uint32 pixel)
{
	Uint32 *pixels = (Uint32 *)surface->pixels;
	pixels[(y * surface->w) + x] = pixel;
}


void BoundaryFill4(SDL_Window *win, Vector2D startPoint, Uint32 pixel_format,
	SDL_Renderer *ren, SDL_Color fillColor, SDL_Color boundaryColor)
{
	SDL_Surface *surface = getPixels(win, ren);

	stack<Vector2D> st;
	Vector2D current;
	st.push(startPoint);

	while (!st.empty())
	{
		current.set(st.top());
		st.pop();

		// out of window area
		if (current.x < 0 || current.x >= surface->w || current.y < 0 || current.y >= surface->h)
			continue;

		Uint32 pixel = get_pixel32(surface, current.x, current.y);
		SDL_Color color = getPixelColor(pixel_format, pixel);
		cout << current.x << ' ' << current.y << endl;

		if (!compareTwoColors(color, boundaryColor))
		{
			//pixel = fillColor.r << 32 | fillColor.g << 8 | fillColor.b;
			Uint32 fillPixel = SDL_MapRGBA(surface->format, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
			put_pixel32(surface, current.x, current.y, fillPixel);

			st.push(Vector2D(current.x, current.y + 1));
			st.push(Vector2D(current.x, current.y - 1));
			st.push(Vector2D(current.x + 1, current.y));
			st.push(Vector2D(current.x - 1, current.y));
		}
	}
}

//======================================================================================================================
//=============================================FILLING TRIANGLE=========================================================

int maxIn3(int a, int b, int c)
{
	int lc = a;
	if (b > lc)
		c = b;
	if (c > lc)
		lc = c;
	return lc;
}

int minIn3(int a, int b, int c)
{
	int lc = a;
	if (b < lc)
		c = b;
	if (c < lc)
		lc = c;
	return lc;
}

void swap(Vector2D &a, Vector2D &b)
{
	Vector2D temp(a);
	a.set(b);
	b.set(temp);
}

void ascendingSort(Vector2D &v1, Vector2D &v2, Vector2D &v3)
{
	if (v1.y > v2.y) {
		swap(v1, v2);
	}

	if (v1.y > v3.y)
	{
		swap(v1, v3);
	}

	if (v2.y > v3.y)
	{
		swap(v2, v3);
	}
}

void TriangleFill1(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	SDL_RenderDrawLine(ren, int(v1.x + 0.5), v1.y, int(v3.x + 0.5), v3.y);
}

void TriangleFill2(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	float c1 = (float)(v3.x - v1.x) / (v3.y - v1.y);
	float c2 = (float)(v3.x - v2.x) / (v3.y - v2.y);
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	float xLeft = v1.x;
	float xRight = v2.x;
	for (int y = v1.y; y <= v3.y; y++)
	{
		xLeft += c1;
		xRight += c2;
		SDL_RenderDrawLine(ren, int(xLeft + 0.5), y, int(xRight + 0.5), y);
	}
}

void TriangleFill3(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	float c1 = (float)(v2.x - v1.x) / (v2.y - v1.y);
	float c2 = (float)(v3.x - v1.x) / (v3.y - v1.y);
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	float xLeft = v2.x;
	float xRight = v3.x;
	for (int y = v3.y; y >= v1.y; y--)
	{
		xLeft -= c1;
		xRight -= c2;
		SDL_RenderDrawLine(ren, int(xLeft + 0.5), y, int(xRight + 0.5), y);
	}
}

void TriangleFill4(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	Vector2D v4;
	v4.x = v1.x + ((v2.y - v1.y) / (v3.y - v1.y)) * (v3.x - v1.x);
	v4.y = v2.y;
	if (v4.x < v2.x)
		swap(v4, v2);
	TriangleFill2(v2, v4, v3, ren, fillColor);
	TriangleFill3(v1, v2, v4, ren, fillColor);
}

void TriangleFill(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	ascendingSort(v1, v2, v3);

	if (v1.y == v2.y && v2.y == v3.y)
		TriangleFill1(v1, v2, v3, ren, fillColor);
	else if (v1.y == v2.y && v1.y < v3.y)
		TriangleFill2(v1, v2, v3, ren, fillColor);
	else if (v2.y == v3.y && v1.y < v3.y)
		TriangleFill3(v1, v2, v3, ren, fillColor);
	else
		TriangleFill4(v1, v2, v3, ren, fillColor);
}
//======================================================================================================================
//===================================CIRCLE - RECTANGLE - ELLIPSE=======================================================
bool isInsideCircle(int xc, int yc, int R, int x, int y)
{
	return (sqrt(pow(xc - x, 2) + pow(yc - y, 2)) <= R);
}

void FillIntersection(int x1, int y1, int x2, int y2, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	if (y1 == y2)
	{
		int y = y1;
		int xmin = min(x1, x2);
		int xmax = max(x1, x2);
		for (int x = xmin; x <= xmax; x++)
			if (isInsideCircle(xc, yc, R, x, y))
				SDL_RenderDrawPoint(ren, x, y);
	}
	else
	{
		int x = x1;
		int ymin = min(y1, y2);
		int ymax = max(y1, y2);
		for (int y = ymin; y <= ymax; y++)
			if (isInsideCircle(xc, yc, R, x, y))
				SDL_RenderDrawPoint(ren, x, y);
	}
}

void FillIntersectionRectangleCircle(Vector2D vTopLeft, Vector2D vBottomRight, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	int ymin = vTopLeft.y;
	int ymax = vBottomRight.y;
	int xmin = vTopLeft.x;
	int xmax = vBottomRight.x;

	for (int y = ymin; y <= ymax; y++)
		FillIntersection(xmin, y, xmax, y, xc, yc, R, ren, fillColor);
}

void RectangleFill(Vector2D vTopLeft, Vector2D vBottomRight, SDL_Renderer *ren, SDL_Color fillColor)
{
	int ymin = vTopLeft.y;
	int ymax = vBottomRight.y;
	int xmin = vTopLeft.x;
	int xmax = vBottomRight.x;

	for (int y = ymin; y <= ymax; y++)
		SDL_RenderDrawLine(ren, xmin, y, xmax, y);
}

void put4line(int xc, int yc, int x, int y, SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	SDL_RenderDrawLine(ren, xc + x, yc + y, xc - x, yc + y);
	SDL_RenderDrawLine(ren, xc + y, yc + x, xc - y, yc + x);
	SDL_RenderDrawLine(ren, xc + y, yc - x, xc - y, yc - x);
	SDL_RenderDrawLine(ren, xc + x, yc - y, xc - x, yc - y);
}

void CircleFill(int xc, int yc, int R, SDL_Renderer *ren, SDL_Color fillColor)
{
	int x = 0;
	int y = R;
	int p = 1 - R;
	put4line(xc, yc, x, y, ren, fillColor);
	while (x < y)
	{
		if (p < 0)
			p += 2 * x + 3;
		else
		{
			p += 2 * (x - y) + 5;
			y--;
		}
		x++;
		put4line(xc, yc, x, y, ren, fillColor);
	}
}

void FillIntersectionEllipseCircle(int xcE, int ycE, int a, int b, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{

}

void FillIntersectionTwoCircles(int xc1, int yc1, int R1, int xc2, int yc2, int R2,
	SDL_Renderer *ren, SDL_Color fillColor)
{
}
